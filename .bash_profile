RED='\033[0;31m'
GRAY='\033[0;37m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

PS1="${GRAY}\h:${YELLOW}\W${NC}$ "


alias ls='ls -G'
printf "Current directory: ${RED}$(pwd)${NC}\n\n"

export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

scrape() {
  wget \
     --recursive \
     --no-clobber \
     --page-requisites \
     --html-extension \
     --convert-links \
     --restrict-file-names=windows \
     --domains $1 \
     --no-parent \
         $1
}

ip() {
  dig +short $1
}

# s-search from https://github.com/zquestz/s/blob/master/README.md
if [ -f $GOPATH/src/github.com/zquestz/s/autocomplete/s-completion.bash ]; then
    . $GOPATH/src/github.com/zquestz/s/autocomplete/s-completion.bash
fi
alias sw="s -p wikipedia"
alias sg="s -p google"

alias type="typerace p"


alias status="curl -I"
